$(document).ready(function() {
    
    const $toggleSwitch = $('#checkbox');
    const $home = $('body, nav ul');
    const $nav = $('ul li a, .menu-btn i, .sign-up');
    const $titik = $('ellipse');
    const $button = $('.btn-daftar');
    const $form = $('.form-control');
    const $form_sucsess = $('.thanks');
  

    $toggleSwitch.on('change', function() {
      if ($toggleSwitch.is(':checked')) {

        $home.css({
            backgroundColor: '#011937',
            color: 'white'
          })
        
        $nav.css ({
          color:'white'
        })
        $titik.css ({
          fill:'white'
        })
        $button.css ({
          backgroundColor: '#63d525',
          color : 'white',
        })
        $form.css ({
          backgroundColor: 'white',
        })
        $form_sucsess.css ({
          color: 'yellow'
        })

    
      } else {
      
        $home.css({
            backgroundColor: 'white',
            color: 'black'
          })

          $nav.css ({
            color:'black'
          })

          $titik.css ({
            fill:'black'
          })

          $button.css ({
            backgroundColor: 'black',
            color : 'white',
          })
          $form.css ({
            backgroundColor: '#D9D9D9',
          })
          $form_sucsess.css ({
            color: 'green'
          })

        
      }
    });
  });

  $(".js-scroll-trigger").click(function(){
    $(".menu-btn").click();
  });