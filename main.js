$(document).ready(function() {
    
    const $toggleSwitch = $('#checkbox');
    const $home = $('body, nav ul');
    const $nav = $('ul li a, .menu-btn i, .sign-up');
    const $titik = $('ellipse');
    const $button = $('.btn-daftar');
    const $icon = $('.benefit-text path');
    const $town = $('.town');
  
    

  
    $toggleSwitch.on('change', function() {
      if ($toggleSwitch.is(':checked')) {

        $home.css({
            backgroundColor: '#011937',
            color: 'white'
          })
        
        $nav.css ({
          color:'white'
        })
        $titik.css ({
          fill:'white'
        })
        $button.css ({
          backgroundColor: '#63d525',
          color : 'white',
        })

        $icon.css ({
          fill:'white',
          stroke: 'black'
        })

        $town.css ({
          fill:'#011937'
        })
  
      } else {
      
        $home.css({
            backgroundColor: 'white',
            color: 'black'
          })

          $nav.css ({
            color:'black'
          })

          $titik.css ({
            fill:'black'
          })

          $button.css ({
            backgroundColor: 'black',
            color : 'white',
          })

          $icon.css ({
            fill:'black',
            stroke: 'white'
          })

          $town.css ({
            fill:'black'
          })
  
      }
    });
  });

  $(".js-scroll-trigger").click(function(){
    $(".menu-btn").click();
  });